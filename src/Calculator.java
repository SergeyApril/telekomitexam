import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.StringTokenizer;

public class Calculator {
    public String evaluate(String l){
        String _expression = l;

        try {
            _expression = _mainParse(_expression);
            DecimalFormat df = new DecimalFormat("###.####");
            return df.format(calculate(_expression));
        } catch (Exception e) {
            return null;
        }
    }
    
    private static String _mainParse(String _expression) throws Exception {
        StringBuilder mainStack = new StringBuilder(""), stringBuildOut = new StringBuilder("");
        char _cInput, _aVar;

        for (int i = 0; i < _expression.length(); i++) {
            _cInput = _expression.charAt(i);
            if (isOperation(_cInput)) {
                while (mainStack.length() > 0) {
                    _aVar = mainStack.substring(mainStack.length()-1).charAt(0);
                    if (isOperation(_aVar) && (opPrior(_cInput) <= opPrior(_aVar))) {
                        stringBuildOut.append(" ").append(_aVar).append(" ");
                        mainStack.setLength(mainStack.length()-1);
                    } else {
                        stringBuildOut.append(" ");
                        break;
                    }
                }
                stringBuildOut.append(" ");
                mainStack.append(_cInput);
            } else if ('(' == _cInput) {
                mainStack.append(_cInput);
            } else if (')' == _cInput) {
                _aVar = mainStack.substring(mainStack.length()-1).charAt(0);
                while ('(' != _aVar) {
                    if (mainStack.length() < 1) {
                        throw new Exception("null");
                    }
                    stringBuildOut.append(" ").append(_aVar);
                    mainStack.setLength(mainStack.length()-1);
                    _aVar = mainStack.substring(mainStack.length()-1).charAt(0);
                }
                mainStack.setLength(mainStack.length()-1);
            } else {
                stringBuildOut.append(_cInput);
            }
        }
        while (mainStack.length() > 0) {
            stringBuildOut.append(" ").append(mainStack.substring(mainStack.length()-1));
            mainStack.setLength(mainStack.length()-1);
        }

        return  stringBuildOut.toString();
    }

    private static boolean isOperation(char isOp) {
        switch (isOp) {
            case '-':
            case '+':
            case '*':
            case '/':
                return true;
        }
        return false;
    }
    private static byte opPrior(char cPriority) {
        switch (cPriority) {
            case '*':
            case '/':
                return 2;
        }
        return 1; 
    }
    
    private static double calculate(String _expression) throws Exception {
        double _varA = 0, _varB = 0;
        String _stringVar;
        Deque<Double> stack = new ArrayDeque<>();
        StringTokenizer st = new StringTokenizer(_expression);
        while(st.hasMoreTokens()) {
            try {
                _stringVar = st.nextToken().trim();
                if (1 == _stringVar.length() && isOperation(_stringVar.charAt(0))) {
                    if (stack.size() < 2) {
                        throw new Exception("null");
                    }
                    _varB = stack.pop();
                    _varA = stack.pop();
                    switch (_stringVar.charAt(0)) {
                        case '+':
                            _varA += _varB;
                            break;
                        case '-':
                            _varA -= _varB;
                            break;
                        case '/':
                            _varA /= _varB;
                            break;
                        case '*':
                            _varA *= _varB;
                            break;
                        default:
                            throw new Exception("null");
                    }
                    stack.push(_varA);
                } else {
                    _varA = Double.parseDouble(_stringVar);
                    stack.push(_varA);
                }
            } catch (Exception e) {
                throw new Exception("null");
            }
        }
        if (stack.size() > 1) {
            throw new Exception("null");
        }

        return stack.pop();
    }
}
