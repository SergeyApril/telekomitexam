import java.util.List;

public class Subsequence {

    public Boolean find(List list1,List list2)
    {
        Boolean result =false;
        if ((list1.size() > 0) & (list2.size() > 0))
        {
            try
            {
                Integer.parseInt(list1.get(0).toString());
                result = findInt(list1, list2);
            } catch (Exception e)
            {
                result = findInt2(list1,list2);
            }


        }
        return result;
    }
    private Boolean findInt(List<Integer>list1, List<Integer> list2){
        Integer k;
        Integer _sizeCount = 0;
        Integer _elementPosition = -1, _tumbler = -1;
        if (list1.size() <= list2.size()) {
            for (Integer n : list1) {
                for (Integer i = 0; i < list2.size(); i++) {
                    k = list2.get(i);
                    if (n.equals(k) & (i > _elementPosition) & _tumbler == -1) {
                        _elementPosition = i;
                        _tumbler = 0;
                        _sizeCount++;
                        if (_sizeCount.equals(list1.size())) {
                            return true;
                        }
                    } else if ((!n.equals(k)) & (_tumbler == -1)) {
                    }
                }
                _tumbler = -1;
            }
        }
        return false;
    }
    private Boolean findInt2 (List<String>list1,List<String> list2){
        String k;
        Integer _sizeCount = 0;
        Integer _elementPosition = -1, _tumbler = -1;
        if (list1.size() <= list2.size()) {
            for (String n : list1) {
                for (Integer i = 0; i < list2.size(); i++) {
                    k = list2.get(i);
                    if (n.equals(k) & (i > _elementPosition) & _tumbler == -1) {
                        _elementPosition = i;
                        _tumbler = 0;
                        _sizeCount++;
                        if (_sizeCount.equals(list1.size())) {
                            return true;
                        }
                    } else if ((!n.equals(k)) & (_tumbler == -1)) {
                    }
                }
                _tumbler = -1;
            }
        }
        return false;
    }



}
